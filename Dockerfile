# This is based on the Dockerfiles from
#   https://arow.info/blog/posts/2017-03-30-servant-on-heroku.html
#   http://mashina.io/psto/docker-haskell
# but all mistakes are mine
#

FROM heroku/heroku:20 AS build

ENV LANG C.UTF-8

# Set up needed code for using Stack and the installation
#
RUN apt-get update \
  && apt-get remove -y --assume-yes imagemagick imagemagick-6-common \
  && apt-get upgrade -y --assume-yes \
  && apt-get install -y --assume-yes \
  g++ \
  gcc \
  libc6-dev \
  libffi-dev \
  libgmp-dev \
  make \
  xz-utils \
  zlib1g-dev \
  git \
  gnupg

# Install stack to /opt/stack/bin.
RUN mkdir -p /opt/stack/bin
RUN curl -L https://get.haskellstack.org/stable/linux-x86_64.tar.gz | tar xz --wildcards --strip-components=1 -C /opt/stack/bin '*/stack'

RUN mkdir -p /opt/starname/src
RUN mkdir -p /opt/starname/bin
WORKDIR /opt/starname/src

# Copy over files in stages to avoid unnescessary rebuilds; the assumption
# is that stack.yaml changes less than the cabal file, which is less than
# the code itself. It seems that changes to stack have made this less
# obvious a "win".
#
ENV PATH "$PATH:/opt/stack/bin:/opt/starname/bin"

COPY ./stack.yaml /opt/starname/src/stack.yaml
COPY ./starname.cabal /opt/starname/src/starname.cabal
RUN stack --no-terminal setup

RUN stack --no-terminal test --only-dependencies

# Only copy over what we need
COPY app/ /opt/starname/src/app/

RUN stack --no-terminal build
RUN stack --no-terminal --local-bin-path /opt/starname/bin install

# Report the ghc and stack versions (after the build so it gets reported
# each time)
RUN stack exec ghc -- --version
RUN stack --version

FROM heroku/heroku:20 AS deploy

ENV LANG C.UTF-8

# Copy over the files we want
COPY --from=build /opt/starname/bin/ /opt/starname/bin/

# Occasionally I review the output of 'apt list' and add or remove
# packages. Not exactly robust
#
# This used to include imagemagick but that caused some problems
# (an update lead to confusion because the configuration file had been
#  updated, which meant that the update failed), so I have removed it
# above.
#
#  imagemagick \
#
# Then I noticed complaints about removing fonts-dejavu-core
# (a requirement for fontconfig-config)
#
RUN apt-get remove -y --assume-yes \
  ghostscript \
  geoip-database \
  ruby \
  rake \
  gcc \
  make \
  xz-utils \
  git \
  telnet \
  ed \
  wget \
  curl \
  unzip \
  zip \
  bzip2 \
  perl \
  python3 \
  mysql-common \
  openssh-client \
  openssh-server \
  gsfonts \
  rsync \
  mtools \
  libxcb1 \
  libxml2 \
  && apt-get autoremove -y --assume-yes \
  && apt-get purge -y --assume-yes \
  && apt-get clean \
  && apt-get autoclean

# Ensure what we do have left is up to date.
#
RUN apt-get update \
  && apt-get upgrade -y --assume-yes

# Remove apt caches to reduce the size of our container.
#
RUN rm -rf /var/lib/apt/lists/*

RUN useradd -ms /bin/bash webserver
RUN chown -R webserver:webserver /opt/starname
USER webserver
# ENV PATH "$PATH:/opt/starname/bin"

WORKDIR /opt/starname

CMD /opt/starname/bin/starname
