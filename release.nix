# See https://github.com/utdemir/hs-nix-template
#
{ compiler ? "ghc8107" }:

let
  isDefaultCompiler = compiler == "ghc8107";
  
  sources = import nix/sources.nix;
  pkgs = import sources.nixpkgs {};

  gitignore = pkgs.nix-gitignore.gitignoreSourcePure [ ./.gitignore ];

  myHaskellPackages = pkgs.haskell.packages.${compiler}.override {
    overrides = hself: hsuper: {

      # At the moment we want to switch to aeson 2 but it's not in stackage
      # which means doing some work. Unfortunately I haven't been able to
      # work out how to do this: I think callHackageDirect should be downloading
      # the requested build from hackage rather than callHackage which only
      # allows "supported" version (however it knows what versions to use),
      # but I can't seem to get it to build.
      #
      # So this means we can't use nix-build for now.
      #

      # hackage tar files are
      #
      #   https://hackage.haskell.org/package/<name>-<version>/<name>-<version>.tar.gz
      #
      # and you may need to bother with callHackageDirect rather than callHackage
      # if the package is relatively new. You will also need
      #
      #   % nix-shell -p nix-prefetch-scripts
      #   % nix-prefetch-url https://...
      #
      # but then it didn't work (but at least told me the value to use).
      #
      #"lens" = hself.callHackage "lens" "5.1" {};
      #"lens" = hself.callHackageDirect {
      #  pkg = "lens";
      #  ver = "5.1";
      #  sha256 = "1l77zaz4g4l7kf1gmdjal77c199j67hy78x48svzsxy9sc5i8p1v";
      #} {};
      #

      # "http2" = hself.callHackage "http2" "3.0.3" {};
      #"http2" = hself.callHackageDirect {
      #  pkg = "http2";
      #  ver = "3.0.3";
      #  sha256 = "1l77zaz4g4l7kf1gmdjal77c199j67hy78x48svzsxy9sc5i8p1v";
      #} {};

      #"hashable" = hself.callHackage "hashable" "1.3.5.0" {};  # latest is 1.4.0.2
      #"semialign" = hself.callHackage "semialign" "1.2.0.1" {};
      #"time-compat" = hself.callHackage "time-compat" "1.9.6.1" {};
      #"aeson" = hself.callHackage "aeson" "2.0.1.0" {};  # could not pick up a later version
      #"aeson" = hself.callHackageDirect {
      #  pkg = "aeson";
      #  ver = "2.0.3.0";
      #  sha256 = "1l77zaz4g4l7kf1gmdjal77c199j67hy78x48svzsxy9sc5i8p1v";
      #} {};

      #"HsYAML-aeson" = hself.callHackage "HsYAML-aeson" "0.2.0.1" {};

      "starname" = hself.callCabal2nix "starname" (gitignore ./.) {};
    };
  };

  shell = myHaskellPackages.shellFor {
    packages = p: [
      p."starname"
    ];
    buildInputs = [
      pkgs.haskellPackages.cabal-install
      pkgs.haskellPackages.hlint
      pkgs.niv
      pkgs.heroku
      pkgs.git
    ] ++ pkgs.lib.optionals isDefaultCompiler [
      # myHaskellPackages.haskell-language-server
    ];
    withHoogle = isDefaultCompiler;
  };

  exe = pkgs.haskell.lib.justStaticExecutables (myHaskellPackages."starname");

  docker = pkgs.dockerTools.buildImage {
    name = "starname";
    config.Cmd = [ "${exe}/bin/starname" ];
  };

in
{
  inherit shell;
  inherit exe;
  inherit docker;
  inherit myHaskellPackages;
  "starname" = myHaskellPackages."starname";
}
