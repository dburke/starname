{-# LANGUAGE OverloadedStrings #-}

{-

We could use an external store to cache the searches, but for now just keep
it all in memory. This means that each time the server is re-stared we lose
the cache, but for now that feels okay.

-}

module Main where

import qualified Data.Aeson as A
import qualified Data.Aeson.Key as Key
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Lazy as L
import qualified Network.Wreq.Session as S

import Control.Concurrent.MVar (MVar, newMVar, modifyMVar_, readMVar)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader (ReaderT, ask, runReaderT)

import Data.Aeson ((.=), object)
import Data.Char (isSpace)
import Data.Default (def)
import Data.Either (partitionEithers)
import Data.Time (UTCTime, diffUTCTime, getCurrentTime)

import Network.HTTP.Types (StdMethod(HEAD))
import Network.Wai.Handler.Warp (defaultSettings, setPort)

import System.Environment (lookupEnv)
import System.Exit (exitFailure)
import System.IO (hFlush, stderr)

import Text.Read (readMaybe)

import Web.Scotty.Trans (ActionT, Options, ScottyT, addroute, get, json, notFound, param, scottyOptsT, setHeader, settings, verbose)

import Resolver (Location(..), identifyName, getResponse)
import Control.Monad.IO.Class (liftIO)

{-
We use the search term after

 - stripping of blank spaces
 - converting to lower case

as the key. If the value is Just then the result was found, otherwise
not.

Note that a Nothing value can indicate "not known" or can indicate
"unable to query the services". This is not ideal.

-}
type Key = T.Text
type LocationCache = M.Map Key (Maybe Location)


-- Strip out white space and convert to "lower case"
cleanKey :: T.Text -> Key
cleanKey t = T.toCaseFold (T.filter (not . isSpace) t)


updateLocationCache ::
  T.Text   -- search term (used in the search)
  -> Maybe Location
  -> LocationCache
  -> LocationCache
updateLocationCache key = M.insert (cleanKey key)

  
lookupLocationCache ::
  T.Text   -- search term (used in the search)
  -> LocationCache
  -> Maybe (Maybe Location)
lookupLocationCache key = M.lookup (cleanKey key)

  
-- Is this a good idea?
--
data StarData =
  StarData { starSession :: S.Session
           , starCache :: MVar LocationCache
           }


type StarApp = ReaderT StarData IO
type ScottyM = ScottyT L.Text StarApp
type ActionM = ActionT L.Text StarApp


makeStarData :: IO StarData
makeStarData = do
  session <- S.newAPISession
  cache <- newMVar mempty
  pure $ StarData session cache

  
production ::
  Int  -- ^ The port number to use
  -> Options
production p = def { verbose = 0
                   , settings = setPort p defaultSettings }

development :: Options
development = def

uerror :: T.Text -> IO ()
uerror msg = do
  T.hPutStrLn stderr ("ERROR: " <> msg)
  hFlush stderr
  exitFailure


main :: IO ()
main = do
  
  mports <- lookupEnv "PORT"
  let eopts = case mports of
        Just ports -> case readMaybe ports of
          Just port ->  Right (production port)
          _ -> Left ("Invalid PORT argument: " <> T.pack ports)

        _ -> Right development

  case eopts of
    Left emsg -> uerror emsg
    Right opts -> do

      starData <- makeStarData
      let wrap r = runReaderT r starData
      startTime <- getCurrentTime
      scottyOptsT opts wrap (webapp startTime)


toLoc :: Location -> A.Value
toLoc (Location ra dec csys service _) =
  object [ "ra" .= ra
         , "dec" .= dec
         , "csys" .= csys
         , "service" .= service
         ]
 

{-
We cache results. Note that there is the possibility that we
could end up adding the same data multiple times, as we
use readMVar on the cache when checking if it is known.

The hope is that we don't get different results from calls
at roughly the same time.

-}

searchForName :: T.Text -> ActionM ()
searchForName name = do

  app <- lift ask
  let session = starSession app
      cache = starCache app

  -- For now allow anyone to call this
  setHeader "Access-Control-Allow-Origin" "*"
  setHeader "Content-Type" "application/json; charset=utf-8"
  
  -- do we know about this term?
  --
  omap <- liftIO (readMVar cache)
  case lookupLocationCache name omap of
    Just (Just loc) -> do
      logIt ("FOUND CACHED HIT: " <> name)
      json (object [ "status" .= True
                   , "name" .= name
                   , "location" .= toLoc loc])

    Just Nothing -> do
      logIt ("FOUND CACHED MISS: " <> name)
      json (object [ "status" .= False ])

    Nothing -> do
      rsp <- liftIO (identifyName session (T.unpack name))
      case getResponse rsp of
        Just loc -> do
          liftIO (modifyMVar_ cache (pure . updateLocationCache name (Just loc)))
          logIt ("STORING HIT: " <> name)
          json (object [ "status" .= True
                       , "name" .= name
                       , "location" .= toLoc loc])

        _ -> do
          liftIO (modifyMVar_ cache (pure . updateLocationCache name Nothing))
          logIt ("STORING MISS: " <> name)
          json (object [ "status" .= False ])


-- For now just dump the results as JSON
--
showCache :: UTCTime -> ActionM ()
showCache startTime = do
  
  app <- lift ask
  let cache = starCache app

  setHeader "Content-Type" "application/json; charset=utf-8"

  omap <- liftIO (readMVar cache)
  let matches = M.toAscList omap
      split (k, Nothing) = Left k
      split (k, Just v) = Right (Key.fromText k, toLoc v)
      (misses, hits) = partitionEithers (map split matches)

      nmisses = length misses
      nhits = length hits

  thisTime <- liftIO getCurrentTime
  json (object [ "nhits" .= nhits
               , "nmisses" .= nmisses
               , "hits" .= object hits
               , "misses" .= misses
               , "uptime" .= diffUTCTime thisTime startTime
               ])
  

logIt :: T.Text -> ActionM ()
logIt msg = liftIO (T.putStrLn ("# LOG: " <> msg))


webapp :: UTCTime -> ScottyM ()
webapp startTime = do

  get "/name/:name" $ do
    name <- param "name"
    searchForName name

  get "/cache" (showCache startTime)
      
  addroute HEAD "/name/:name" $ do
    setHeader "Access-Control-Allow-Origin" "*"
    setHeader "Content-Type" "application/json; charset=utf-8"

  addroute HEAD "/cache" $ do
    setHeader "Content-Type" "application/json; charset=utf-8"

  notFound $ do
    setHeader "Content-Type" "application/json; charset=utf-8"
    json ("Not Found" :: T.Text)
  
