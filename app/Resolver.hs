{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}

module Resolver (Response(..)
                , Location(..)
                , getResponse
                , isInvalidName
                , identifyName
                ) where

import qualified Data.ByteString.Lazy as LB
import qualified Data.ByteString.Lazy.Char8 as LB8
import qualified Network.Wreq as W
import qualified Network.Wreq.Session as S
import qualified Text.XML.Light.Proc as P

import Control.Exception (Exception, catch, throw)
import Control.Lens ((^.), (&), (.~))
import Control.Monad (when)

import Data.Functor ((<&>))
-- import Data.Maybe (isJust, mapMaybe)
import Data.Maybe (mapMaybe)
import Data.Typeable (Typeable)

import Network.HTTP.Client (HttpException(HttpExceptionRequest), HttpExceptionContent(StatusCodeException))
import Network.HTTP.Types.Status (mkStatus)
import Network.URI (URI, escapeURIString, isUnescapedInURIComponent, parseURI)

import System.IO (hPutStrLn, stderr)

import Text.Read (readMaybe)
import Text.XML.Light (unqual)
import Text.XML.Light.Input (parseXML)


combine :: String -> String -> Maybe URI
combine base segment =
  let conv = escapeURIString isUnescapedInURIComponent segment
  in parseURI (base <> conv)

{-

This module provides a simple interface - the identify_name routine -
to a name resolver, which is currently
the one provided by the CADC at
https://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/cadc-target-resolver/

For cases when the CADC resolver is not available it falls back to
using the Sesame interface from CDS: http://vizier.u-strasbg.fr/vizier/doc/sesame.htx

-}

data NameError
  = InvalidURL  -- unable to form URL
  | ProblemWithCall HttpException
  | InvalidResponse LB.ByteString
  | NoMatch
  deriving (Show, Typeable)

instance Exception NameError

data Resolver = CADC | Sesame
  deriving (Eq, Show)

data Location =
  Location { lra :: Double
           , ldec :: Double
           , lcoord :: String   -- generally assumed to be ICRS
           , lservice :: String -- free-form string from the calling service
           , lcaller :: Resolver
           }
  deriving Show


query :: S.Session -> Maybe URI -> IO LB.ByteString
query session murl = do
  url <- case murl of
           Nothing -> throw InvalidURL
           Just u -> pure u

  -- Convert the HttpExceptionRequest to our error. We want to
  -- convert a status=425 error (for CADC) to a NoMatch case,
  -- otherwise we just wrap up the error. We assume here that
  -- Sesame does not raise a 425 error so we can just check for it
  --
  let errHandle (e :: HttpException) = do
        -- hPutStrLn stderr $ "# ERROR: HTTP exception\n" ++ show e
        case e of
          HttpExceptionRequest _ (StatusCodeException resp _) | resp ^. W.responseStatus == status425 -> throw NoMatch
          _ -> throw (ProblemWithCall e)

      status425 = mkStatus 425 mempty

      opts = W.defaults & W.header "User-Agent" .~ ["starname 0.1 Doug Burke dburke.gw@gmail.com"]

      get = S.getWith opts session (show url)
    
  resp <- catch get errHandle
  pure (resp ^. W.responseBody)


-- CADC resonds with a 425 to indicate "no match", and this gets
-- converted to a HttpException, so if we get a response we
-- expect it to contain data.
--
identifyNameCADC :: S.Session -> String -> IO Location
identifyNameCADC session name =
  let murl = combine "https://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/cadc-target-resolver/find?format=ascii&service=all&cached=true&target=" name
  in do
    body <- query session murl

    -- The response should be lines of a=b. Looking for
    --    service
    --    coordsys
    --    ra
    --    dec
    -- Do not assume an order in case it changes.
    --
    -- The body seems to use \r\n format so we need to drop the \r
    -- character.
    --
    let ls = lines (LB8.unpack body)
        split txt = case span (/= '=') txt of
          (_, "") -> Nothing
          ("", _) -> Nothing
          (l, r) -> if l `elem` ["service", "coordsys", "ra", "dec"]
                    then Just (l, takeWhile (/= '\r') (drop 1 r))
                    else Nothing
          
        toks = mapMaybe split ls

        mra = lookup "ra" toks >>= readMaybe
        mdec = lookup "dec" toks >>= readMaybe
        mcoord = lookup "coordsys" toks
        mservice = lookup "service" toks

        mloc = Location <$> mra <*> mdec <*> mcoord <*> mservice <*> pure CADC

    case mloc of
      Just loc -> pure loc
      _ -> throw (InvalidResponse body)

-- See http://vizier.u-strasbg.fr/vizier/doc/sesame.htx
--
-- Allow the use of cached data.
--
identifyNameSesame :: S.Session -> String -> IO Location
identifyNameSesame session name = 
  let murl = combine "https://cdsweb.u-strasbg.fr/cgi-bin/nph-sesame/-ox/SNV?" name
  in do

    body <- query session murl

    let xdata = parseXML body

        -- looking for either
        --   Sesame/Target/INFO
        --
        --   Sesame/Target/Resolver [name=]/
        --     jradeg
        --     jdedeg
        -- and assume this is ICRS
        --
        -- There should be only one Resolver element; just pick
        -- the first one in case many are found.
        --
        elems = P.onlyElems xdata
        targets = mapMaybe (P.findElement (unqual "Target")) elems

    when (null targets) $ logIt "NO TARGET" >> throw (InvalidResponse body)

    {- I have no idea why I wanted this
    let target = head targets
    when (isJust (P.findElement (unqual "INFO") target)) $ logIt "NO INFO" >> throw NoMatch
    -}

    let resolvers = mapMaybe (P.findElement (unqual "Resolver")) elems
    when (null resolvers) $ logIt "NO RESOLVER" >> throw (InvalidResponse body)

    let resolver = head resolvers
        mservice = P.findAttr (unqual "name") resolver
        mra = P.findChild (unqual "jradeg") resolver >>= readMaybe . P.strContent
        mdec = P.findChild (unqual "jdedeg") resolver >>= readMaybe . P.strContent

        mloc = Location <$> mra <*> mdec <*> pure "ICRS" <*> mservice <*> pure Sesame

    case mloc of
      Just loc -> pure loc
      _ -> logIt "PROBLEM DECODING VALUE" >> throw (InvalidResponse body)
      

data Response =
  Success Location
  | InvalidName
  | NetworkError


getResponse :: Response -> Maybe Location
getResponse (Success l) = Just l
getResponse _ = Nothing

isInvalidName :: Response -> Bool
isInvalidName InvalidName = True
isInvalidName _ = False


{-
Would it be useful to know the CADC call was a network error
and the Sesame call failed due to an invalid name? In other words,
for the error condition we only know what the Sesame response was.
-}
identifyName :: S.Session -> String -> IO Response
identifyName session name = do

  let catchError (e :: NameError) = pure (Left e)

  {-

    CADC seems to be failing so drop it for now

  eans <- catch (identifyNameCADC session name <&> Right) catchError
  case eans of
    Left e -> do
      -- logIt $ "Unable to call CADC for '" <> name <> "'"
      -- logIt (show e)

      eans2 <- catch (identifyNameSesame session name <&> Right) catchError
      case eans2 of
        Left NoMatch -> do
          pure InvalidName

        Left e2 -> do
          logIt $ "LOG: unable to call CADC/Sesame for '" <> name <> "'"
          -- logIt (show e2)
          pure NetworkError

        Right ans2 -> pure (Success ans2)
      
    Right ans -> pure (Success ans)
    -}

  eans <- catch (identifyNameSesame session name <&> Right) catchError
  case eans of
    Left NoMatch -> pure InvalidName

    Left e -> do
      logIt $ "LOG: unable to call Sesame for '" <> name <> "'"
      logIt (show e)
      pure NetworkError

    Right ans -> pure (Success ans)

logIt :: String -> IO ()
logIt = hPutStrLn stderr
