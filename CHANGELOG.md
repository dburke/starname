# Revision history for starname

## 0.2.0.0 -- 2022-03-23

* Drop CADC search for now and fix up the SESAME search
  (allow cached data and it seems the URL order has changed)
* Bump to GHC 9.0

## 0.1.0.0 -- 2022-02-02

* First version. Released on an unsuspecting world.
