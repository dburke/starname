#
# This is only for running commands. It is not intended to handle
# dependency resolution - e.g. to avoid a push or build if not needed.
#

APP=namesky

PORT=3000

help:
	@echo "Targets are:"
	@echo "   help        - this page"
	@echo ""
	@echo "   cabal       - build with cabal"
	@echo "   stack       - build with stack"
	@echo ""
	@echo "   docker      - build with docker"
	@echo "   cleandocker - build with docker (clean)"
	@echo "   rundocker   - run the docker build locally"
	@echo "   pushdocker  - build/push the docker build"
	@echo ""

cabal:
	@echo "### Building via cabal"
	@echo "##"
	@cabal build

stack:
	@echo "### Building via stack"
	@echo "##"
	@stack build

docker:
	@echo "### Making docker image"
	@echo "##"
	@echo "## ${APP}"
	@echo "##"
	@sudo docker build -t registry.heroku.com/${APP}/web .

cleandocker:
	@echo "### Making docker image (no cache)"
	@echo "##"
	@echo "## ${APP}"
	@echo "##"
	@sudo docker build --no-cache -t registry.heroku.com/${APP}/web .

rundocker:
	@echo "### Running ${APP} docker image locally"
	@echo "##"
	@echo "## ${APP}"
	@echo "## PORT=${PORT}"
	@echo "##"
	@sudo docker run -it --network host --env PORT=${PORT} registry.heroku.com/${APP}/web

pushdocker:
	@echo "### Pushing docker image to Heroku"
	@echo "##"
	@echo "## - if this fails you may need to"
	@echo "##     sudo heroku container:login"
	@echo "##"
	@echo "## ${APP}"
	@echo "##"
	@sudo heroku container:push web --app ${APP}
	@sudo heroku container:release web --app ${APP}
